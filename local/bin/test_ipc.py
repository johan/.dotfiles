import os
import errno
import time

FIFO = 'komusNamedPipe'
try:
    os.mkfifo(FIFO)
except OSError as e: 
    if e.errno != errno.EEXIST:
        raise e

print("Opening FIFO...")
with open(FIFO) as fifo:
    print("FIFO opened")
    while True:
        data = fifo.read()
        if len(data) == 0:
            time.sleep(0.5)
        print('Read: "{0}"'.format(data))

print("end of read program")
