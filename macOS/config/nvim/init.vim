" Enable mouse support
set mouse=a

" number of spaces per tab
set tabstop=4

" number of spaces in tab when editing
set softtabstop=4

" number of spaces to use for autoindent
set shiftwidth=4

" tabs are spaces
set expandtab

" automatically indent
set autoindent

" copy indent from previous line
set copyindent

set hidden
set showcmd
set wildmenu
set showmatch
set laststatus=0
set nobackup
set noswapfile
