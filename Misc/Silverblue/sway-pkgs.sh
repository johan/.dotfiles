rpm-ostree install --idempotent feh \
fira-code-fonts \
fontawesome-fonts \
powerline-fonts \
light \
mako \
ncmpcpp \
pavucontrol \
nnn \
rofi \
swaylock \
termite \
waybar \
wl-clipboard \
mpc \
mpd \
mpv \
grim \
powertop \
slurp