// User-preferences

user_pref("browser.proton.aboutwelcome.enabled", true);
user_pref("browser.proton.appmenu.enabled", true);
user_pref("browser.proton.contextmenus.enabled", true);
user_pref("browser.proton.enabled", true);
user_pref("browser.proton.infobars.enabled", true);
user_pref("browser.proton.modals.enabled", true);
user_pref("browser.proton.tabs.enabled", true);
user_pref("browser.proton.toolbar.enabled", true);
user_pref("browser.proton.toolbar.version", 3);
user_pref("browser.proton.urlbar.enabled", true);
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.toolbars.bookmarks.visibility", "never");
user_pref("extensions.activeThemeID", "firefox-compact-dark@mozilla.org");
user_pref("gfx.webrender.enabled", true);
user_pref("layout.spellcheckDefault", 0);
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.peerconnection.enabled", false);
user_pref("widget.wayland-dmabuf-vaapi.enabled", true);


