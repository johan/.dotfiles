# Check /etc/zsh/zshrc for system-wide settings. You can override those using this file

if test -d $ZDOTDIR/config.d/; then
	for profile in /$ZDOTDIR/config.d/*.sh; do
		test -r "$profile" && . "$profile"
	done
	unset profile
fi
